use std::env;
use std::fs::File;
use std::io::{self, BufRead, BufReader};
use std::path::PathBuf;

// simple, not very effective tool to distinguish (some) domain names generated with DGAs from
// actual domain names
fn main() -> Result<(), String> {
    let listname = env::args()
        .nth(1)
        .map(|s| PathBuf::from(&s))
        .ok_or_else(|| "missing domain list file name parameter".to_owned())?;

    let file = File::open(listname).map_err(|e| format!("unable to open domain list: {e}"))?;
    let list_reader = BufReader::new(file);

    let (total, generated) =
        count_generated(list_reader).map_err(|e| format!("unable to read domain list: {e}"))?;
    let ratio_percent = generated as f32 / total as f32 * 100.0;

    println!("{generated}/{total} ≈ {ratio_percent:.2}%");

    Ok(())
}

fn count_generated<R: BufRead>(mut reader: R) -> io::Result<(u64, u64)> {
    let mut buf = String::with_capacity(128);
    let mut total: u64 = 0;
    let mut generated: u64 = 0;
    while reader.read_line(&mut buf)? != 0 {
        buf.make_ascii_lowercase();
        total += 1;
        if is_generated(&buf) {
            generated += 1;
        }
        buf.clear();
    }
    Ok((total, generated))
}

const DIGITS_THRESHOLD: usize = 3;
const VOWELLESS_THRESHOLD: u32 = 5;
const ENTROPY_THRESHOLD: f32 = 3.8;
const VOWEL_RATIO_THRESHOLD: f32 = 4.0;

const NDIGITS_SCORE: f32 = 1.0;
const NVOWEL_SCORE: f32 = 1.0;
const NVOWELLESS_SCORE: f32 = 1.0;
const ENTROPY_SCORE: f32 = 1.0;
const CLASSIFIER_THRESHOLD: f32 = 1.0;

fn is_generated(full_domain: &str) -> bool {
    let (domain, _) = full_domain
        .rsplit_once('.')
        .unwrap_or_else(|| (full_domain.trim(), ""));

    let mut score: f32 = 0.0;

    let n_digits = domain.chars().filter(char::is_ascii_digit).count();
    if n_digits > DIGITS_THRESHOLD {
        score += NDIGITS_SCORE;
    }

    let mut len_vowelless_sequence: u32 = 0;
    for c in domain.chars() {
        len_vowelless_sequence += 1;
        if ['a', 'e', 'i', 'o', 'u', '.'].contains(&c) {
            len_vowelless_sequence = 0;
        }
    }
    if len_vowelless_sequence > VOWELLESS_THRESHOLD {
        score += NVOWELLESS_SCORE;
    }

    let n_vowel = domain
        .chars()
        .filter(|c| ['a', 'e', 'i', 'o', 'u'].contains(c))
        .count();
    let n_consonant = domain
        .chars()
        .filter(|c| {
            [
                'b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n', 'p', 'q', 'r', 's', 't',
                'v', 'w', 'x', 'y', 'z',
            ]
            .contains(c)
        })
        .count();

    if n_vowel > 0 && n_consonant as f32 / n_vowel as f32 > VOWEL_RATIO_THRESHOLD {
        score += NVOWEL_SCORE;
    }

    if entropy(domain) > ENTROPY_THRESHOLD {
        score += ENTROPY_SCORE;
    }

    score >= CLASSIFIER_THRESHOLD
}

fn entropy(domain: &str) -> f32 {
    let len = domain.len();
    if len == 0 {
        return 0.0;
    }
    let mut counter: [u16; 128] = [0; 128];
    let mut buf = [0];
    for c in domain.chars().filter(|c| c.is_ascii()) {
        c.encode_utf8(&mut buf);
        counter[buf[0] as usize] += 1;
    }

    -counter
        .into_iter()
        .filter(|count| *count > 0)
        .map(|count| {
            let pk = count as f32 / len as f32;
            pk * pk.log2()
        })
        .sum::<f32>()
}
