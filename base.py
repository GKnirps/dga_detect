#!/usr/bin/env python3

import math
import sys

# In the original context, these values were imports from another fileto give students some leverage on
# adjusting the classifier. For completeness, I left it in here
NVOWEL_SCORE: float = 1.0
NDIGITS_SCORE: float = 1.0
NVOWELLESS_SCORE: float = 1.0
ENTROPY_SCORE: float = 1.0
CLASSIFIER_THRESHOLD: float = 1.0

def entropy(domain: str) -> float:
    counter = [0] * 128
    for c in domain:
        if ord(c) > 127:
            raise Exception(f"non-ascii code point '{c}' in domain '{domain}'")
        counter[ord(c)] += 1

    e = 0.0
    for i in counter:
        if i != 0:
            pk = i / len(domain)
            e += pk * math.log2(pk)

    return -e


DIGITS_THRESHOLD = 3
VOWELLESS_THRESHOLD = 5
ENTROPY_THRESHOLD = 3.8
VOWEL_RATIO_THRESHOLD = 4


def is_generated(full_domain: str) -> bool:
    # We ignore the TLD for this analysis
    domain = full_domain.rsplit(".", 1)[0].lower()
    score = 0.0
    n_digits = sum(map(str.isdigit, domain))
    if n_digits > DIGITS_THRESHOLD:
        score += NDIGITS_SCORE

    len_vowelless_sequence = 0
    for c in domain:
        len_vowelless_sequence += 1
        if c in "aeiou.":
            len_vowelless_sequence = 0

    n_vowel = sum(map(lambda c: c in "aeiou", domain))
    n_consonant = sum(map(lambda c: c in "bcdfghjklmnpqrstvwxyz", domain))
    if n_vowel > 0 and n_consonant / n_vowel > VOWEL_RATIO_THRESHOLD:
        score += NVOWEL_SCORE

    if len_vowelless_sequence > VOWELLESS_THRESHOLD:
        score += NVOWELLESS_SCORE

    if entropy(domain) > ENTROPY_THRESHOLD:
        score += ENTROPY_SCORE

    return score >= CLASSIFIER_THRESHOLD


def classify(file: str):
    domains = []
    with open(file) as f:
        domains = f.readlines()

    n_generated = sum(map(is_generated, domains))
    print(
        f"{n_generated}/{len(domains)} ≈ {n_generated/len(domains) * 100:.01f}% have been detected as generated"
    )


if __name__ == "__main__":
    if len(sys.argv) < 2:
        print(f"usage: {sys.argv[0]} <word list file>")
        sys.exit(1)
    classify(sys.argv[1])
